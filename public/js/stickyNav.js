$(document).ready(function() {
    $(document).scroll(function () {
        var scroll = $(this).scrollTop();
        var topDist = $("#nav-container").position();
        if (scroll > topDist.top - 10) {
           document.getElementById("nav1").className = "main-nav2";
        }
        if (scroll < topDist.top) {
           document.getElementById("nav1").className = "main-nav";
        }
    });
});
