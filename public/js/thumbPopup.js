/* EVENT LISTENER OPENS IMAGE */
let btns = Array.from(document.querySelectorAll(".cell"));
let img = document.querySelector('.popup-image');
let dim = document.getElementById('dim');

btns.forEach((btn) => {
  btn.addEventListener('click', (e) => {
    img.src = btn.dataset.image;

    setTimeout(function(){
      dim.style.display = 'block';
      dim.style.opacity = '1';
      img.style.transform = "scale(1, 1) translate(-50%, -50%)";
    }, 0);
  })
})

/* MINIMIZES IMAGE AND DIM */
function minImage() {
   var elems = document.getElementsByClassName("popup-image");
   for (i = 0; i < elems.length; i++) {
      elems[i].style.transform = 'scale(0,0) translate(-50%, -50%)';
   }
   document.getElementById('dim').style.display = 'none';
}
