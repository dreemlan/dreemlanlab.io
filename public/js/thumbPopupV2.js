var title;
var details;
var img;

function getDetails(clicked) {
  let nameID = $(clicked).attr("id");
  info = document.getElementById(nameID).getElementsByClassName('popup-wrapper')[0];
  img = document.getElementById(nameID).querySelector('.popup-image');

  info.style.transform = "scale(1, 1) translate(-50%, -50%)";
}


/* EVENT LISTENER OPENS IMAGE */
let btns = Array.from(document.querySelectorAll(".cell"));
let dim = document.getElementById('dim');

btns.forEach((btn) => {
  btn.addEventListener('click', (e) => {
    img.src = btn.dataset.image;
    dim.style.display = 'block';

    setTimeout(function(){
      dim.style.opacity = '1';
      img.style.transform = "scale(1, 1)";
    }, 0);
  })
})


/* MINIMIZES IMAGE AND DIM */
function minImage() {
   var elems = document.getElementsByClassName("popup-image");
   for (i = 0; i < elems.length; i++) {
      elems[i].style.transform = 'scale(0,0)';
   }

   info.style.transform = "scale(0, 0) translate(-50%, -50%)";

   dim.style.opacity = '0';
   setTimeout(function(){
     dim.style.display = 'none';
   }, 400);
}
