const button = document.getElementById('toTop');

window.addEventListener('scroll', ()=> {
  if(window.scrollY > 800) {
    button.classList.add('show-button');
  } else {
    button.classList.remove('show-button');
  }
});

$(document).ready(function(){
  // Add smooth scrolling to top
  $("a[href='#myPage']").on('click', function(event) {

  // Make sure this.hash has a value before overriding default behavior
  if (this.hash !== "") {

    // Prevent default anchor click behavior
    event.preventDefault();

    // Store hash
    var hash = this.hash;

    // Using jQuery's animate() method to add smooth page scroll
    // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
    $('html, body').animate({
      scrollTop: $(hash).offset().top
    }, 700, function(){

      // Add hash (#) to URL when done scrolling (default click behavior)
      window.location.hash = hash;
      });
    } // End if
  });
})
